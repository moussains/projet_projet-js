#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Users
#------------------------------------------------------------

CREATE TABLE Users(
        idUsers       Int  Auto_increment  NOT NULL ,
        nom           Varchar (255) NOT NULL ,
        prenom        Varchar (255) NOT NULL ,
        dateNaissance Date NOT NULL ,
        email         Varchar (255) NOT NULL ,
        mdp           Varchar (255) NOT NULL ,
        photo         Varchar (255) NOT NULL ,
        statut        Boolean NOT NULL ,
        dateInscrit   Date NOT NULL
	,CONSTRAINT Users_PK PRIMARY KEY (idUsers)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Message
#------------------------------------------------------------

CREATE TABLE Message(
        idMessage       Int  Auto_increment  NOT NULL ,
        dateM           Datetime NOT NULL ,
        libelleM        Text NOT NULL ,
        statutVue       Boolean NOT NULL ,
        dateVue         Datetime NOT NULL ,
        idUsers1         Int ,
        idUsers2         Int NOT NULL
	,CONSTRAINT Message_PK PRIMARY KEY (idMessage)

	,CONSTRAINT Message_Users1_FK FOREIGN KEY (idUsers1) REFERENCES Users(idUsers)
	,CONSTRAINT Message_Users2_FK FOREIGN KEY (idUsers2) REFERENCES Users(idUsers)
)ENGINE=InnoDB;

