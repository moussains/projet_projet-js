<?php
	require_once "../../server.php";

	$lesMessages = $Message->getMessage($_GET['iduser1'], $_GET['iduser2']);

	foreach ($lesMessages as $lesmsg) { ?>


		<?php if ($lesmsg['idUsers1']==$_GET['iduser1']) { ?>
			<div  class="row justify-content-end ">
				<div class="card-column col-md-7 col-10 mt-2">
					<div class="card bg-warning">
						<div class="card-body">
							<small style="float: right;" id="dateheure"><?php echo $lesmsg['dateM']; ?></small>
							<strong><?php echo $lesmsg['nom'].' '.$lesmsg['prenom']; ?></strong><br>
							<p class="card-text"><?php echo $lesmsg['libelleM']; ?></p>
						</div>
					</div>
					<small style="float: right;"><i></i></small>
				</div>
			</div>
		<?php }else{ ?>

			<div  class="row justify-content-start">
				<div class="card-column col-md-7 col-10 mt-2">
					<div class="card bg-light">
						<div class="card-body">
							<small style="float: right;" id="dateheure"><?php echo $lesmsg['dateM']; ?></small>
							<strong><?php echo $lesmsg['nom'].' '.$lesmsg['prenom']; ?></strong><br>
							<p class="card-text"><?php echo $lesmsg['libelleM']; ?></p>
						</div>
					</div>
					<small><i></i></small>
				</div>
			</div>
		<?php	}	
	} ?>