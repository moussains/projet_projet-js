<?php 
	include "../server.php";
?>

	<?php
		$lesMessages = $Message->getMessage($_GET["le_user1"], $_GET["le_user2"]);
	?>
	<?php foreach ($lesMessages as $messages): ?>
		<?php if ($messages["idU"]==$_GET["le_user1"]): ?>
			<div  class="row justify-content-end ">
				<div class="card-column col-md-7 col-10 mt-2">
					<div class="card bg-warning">
						<div class="card-body">
							<p class="card-text"><?php echo $messages["libelleM"]; ?></p>
						</div>
					</div>
					<small style="float: right;"><i><?php echo $messages["dateM"].", ".$messages["prenom"]; ?></i></small>
				</div>
			</div>
		<?php else : ?>
			<div  class="row justify-content-start">
				<div class="card-column col-md-7 col-10 mt-2">
					<div class="card bg-light">
						<div class="card-body">
							<p class="card-text"><?php echo $messages["libelleM"]; ?></p>
						</div>
					</div>
					<small><i><?php echo $messages["dateM"].", ".$messages["prenom"]; ?></i></small>
				</div>
			</div>
		<?php endif ?>
	<?php endforeach ?>