<?php 
	include "../server.php";
?>
<?php if (isset($_POST["verifMessage"])): ?>
	<?php 
		$leUser = $Users->getUsersById($_POST["le_user2"]);
		$lesMessages = $Message->getMessage($_POST["le_user1"], $_POST["le_user2"]);
	?>
	
	<div class="card-header">
		<span style="float: left;"><img src="../images/<?php echo $leUser["photo"];?>" alt="<?php echo $leUser["nom"]." ".$leUser["prenom"]; ?>" class="mr-3 rounded-circle" style="width:30px;"><?php echo $leUser["nom"]." ".$leUser["prenom"]; ?></span>
		<button class="btn btn-default border btn-sm" style="float: right;"><span class="fa fa-trash-o mt-2"></span></button>
	</div>
	<?php $i=0; foreach ($lesMessages as $messages){$i++;}?>
	<div class="card-body card-tchat <?php if($i<1){echo 'card-vide';}?>">


		<?php foreach ($lesMessages as $messages): ?>
			<?php if ($messages["idU"]==$_POST["le_user1"]): ?>
				<div  class="row justify-content-end ">
					<div class="card-column col-md-7 col-10 mt-2">
						<div class="card bg-warning">
							<div class="card-body">
								<p class="card-text"><?php echo $messages["libelleM"]; ?></p>
							</div>
						</div>
						<small style="float: right;"><i><?php echo $messages["dateM"].", ".$messages["prenom"]; ?></i></small>
					</div>
				</div>
			<?php else : ?>
				<div  class="row justify-content-start">
					<div class="card-column col-md-7 col-10 mt-2">
						<div class="card bg-light">
							<div class="card-body">
								<p class="card-text"><?php echo $messages["libelleM"]; ?></p>
							</div>
						</div>
						<small><i><?php echo $messages["dateM"].", ".$messages["prenom"]; ?></i></small>
					</div>
				</div>
			<?php endif ?>
		<?php endforeach ?>
	</div>

	<div class="card-footer">
		<div class="row">
			<div class="col-md-9">
				<form id="formMessage">
					<textarea class="form-control" rows="2" id="leMessage"></textarea>
				</form>

			</div>
			<div class="col-md-3">
				<button class="btn btn-primary mt-2" onclick="setMessage(<?php echo "2,".$leUser["idUsers"];?>)"><span class="fa fa-send"></span></button>
			</div>
		</div>
	</div>
<?php endif ?>

<?php if (isset($_POST["verifMessageEnvoye"])): ?>
	<?php
		$lemessage = nl2br($_POST["le_message"]);
		$Message->setMessage($lemessage, $_POST["le_user1"], $_POST["le_user2"]);

		$lesMessages = $Message->getMessage($_POST["le_user1"], $_POST["le_user2"]);
	?>
	<?php foreach ($lesMessages as $messages): ?>
		<?php if ($messages["idU"]==$_POST["le_user1"]): ?>
			<div  class="row justify-content-end ">
				<div class="card-column col-md-7 col-10 mt-2">
					<div class="card bg-warning">
						<div class="card-body">
							<p class="card-text"><?php echo $messages["libelleM"]; ?></p>
						</div>
					</div>
					<small style="float: right;"><i><?php echo $messages["dateM"].", ".$messages["prenom"]; ?></i></small>
				</div>
			</div>
		<?php else : ?>
			<div  class="row justify-content-start">
				<div class="card-column col-md-7 col-10 mt-2">
					<div class="card bg-light">
						<div class="card-body">
							<p class="card-text"><?php echo $messages["libelleM"]; ?></p>
						</div>
					</div>
					<small><i><?php echo $messages["dateM"].", ".$messages["prenom"]; ?></i></small>
				</div>
			</div>
		<?php endif ?>
	<?php endforeach ?>
<?php endif ?>

<script type="text/javascript">
	function loadlink(){
	    $('.card-tchat').load(function(){$(this).unwrap()});
	}
	setInterval(function(){
	    loadlink() // this will run after every 5 seconds
	}, 1000);
</script>